provider "aws" {
    access_key = var.aws_access
    secret_key = var.aws_secret
    region = var.aws_region
}

resource "aws_lb_target_group" "my_target_group" {
    health_check {
        interval =              10
        path =                  "/index.html"
        protocol =              "HTTP"
        timeout =               5
        healthy_threshold =     5
        unhealthy_threshold =   2
    }
    name     = "tf-example-lb-tg"
    port     = 80
    protocol = "HTTP"
    vpc_id   = var.vpc_id
}

data "aws_subnets" "example" {
  filter{
    name = "vpc-id"
    values = [var.vpc_id]
  }
}

resource  "aws_lb" "my-aws-alb" {
  name =            "my-terraform-alb"
  internal =        false
  security_groups = var.vpc_security_group
  subnets =        "${data.aws_subnets.example.ids}"
}

resource "aws_lb_listener" "aws_lb_listener_test" {
  load_balancer_arn = aws_lb.my-aws-alb.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.my_target_group.arn
  }
}

data "aws_ami" "ami" {
  most_recent = true
  owners = ["self"]
  filter {
    name = "name"
    values = ["example-ami*"]
  }
}

//data "template_file" "user_data_hw" {
//   template = <<-EOF
//               #!/bin/bash
//               yum install httpd -y
//               service httpd start
//               chkconfig httpd on
//               mkdir /var/www/html
//               echo "This message is from launch_configuration-1" > /var/www/html/index.html
//               EOF 
//}

resource "aws_launch_template" "packer_template" {
  name = "pat-template"
  disable_api_termination = true
  // iam_instance_profile {
  //   name = "profile-1"
  // } # This is IAM Role
  image_id = "${data.aws_ami.ami.id}"
  instance_initiated_shutdown_behavior = "terminate"
  instance_type = "t2.micro"
  key_name = "jenkins-pipelines"
  vpc_security_group_ids = var.vpc_security_group
  tags = {
      Name = "pat-launch-template"
  }
  #user_data = "${base64encode(data.template_file.user_data_hw.rendered)}"    # use base64encode or base64
  lifecycle { 
    create_before_destroy = true
  }
}

data "aws_availability_zones" "all" {}

resource "aws_autoscaling_group" "rolling_deployment_asg" {
  name = "pat-asg-${aws_launch_template.packer_template.latest_version}"
  launch_template {
    id      = "${aws_launch_template.packer_template.id}"
    version = "${aws_launch_template.packer_template.latest_version}"
  }
  min_size             = 1
  desired_capacity	   = 2
  max_size             = 2
  health_check_type = "EC2"
  target_group_arns = [ "${aws_lb_target_group.my_target_group.arn}" ]
  vpc_zone_identifier = "${data.aws_subnets.example.ids}"
  tag {
    key = "Name"
    value = "terraform-pat-asg"
    propagate_at_launch = true
  }
  tag {
    key = "Node"
    value = "Worker"
    propagate_at_launch = true
  }
  instance_refresh {
    strategy = "Rolling"
    preferences {
      min_healthy_percentage = 50
    }
    triggers = ["tag"]
  }
  lifecycle {
    create_before_destroy = true
  }
}

# Links for Launch_Template
# https://stackoverflow.com/questions/60391297/rolling-update-of-asg-using-launch-template
# https://blog.imfiny.com/imfiny-aws-terraform-2019-01-18-aws-launch-templates-html/
