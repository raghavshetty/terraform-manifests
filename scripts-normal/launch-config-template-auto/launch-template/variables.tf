variable "aws_region" {
    default = "ap-south-1"
}

variable "aws_access" {
  default = "*********************************"
}

variable "aws_secret" {
  default = "*********************************"
}

variable "ec2-ami" {
    default = "ami-05102cba58d91b74a"  #ami-0756a1c858554433e-ubuntu  #ami-079b5e5b3971bd10d-amazonlinux-2
}

variable "instance_type" {
    default = "t2.micro"
}

variable  "ec2_keypair" {
    default = "*************"
}

variable "vpc_security_group" {
    default = ["*********************"]
}

variable "subnets" {
    type = list
    default = ["subnet-*****************","subnet-*****************"]
}

variable "vpc_id" {
    default = "vpc-******************"
} 
