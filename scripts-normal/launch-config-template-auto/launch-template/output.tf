// output "publicIp" {
//     value = "${aws_instance.my_first_machine.public_ip}"
// }

// output "curl" {
//     value = "curl http://${aws_instance.my_first_machine.public_ip}"
// }

// output "Login-with-Key" {
//     value = "ssh -i jenkins-pipelines.pem ubuntu@${aws_instance.my_first_machine.public_ip}"
// }

output "alb_dns_name" {
  value = aws_lb.my-aws-alb.dns_name
}
