terraform {
    backend "s3"{
        region = "ap-south-1"
        access_key = "****************"
        secret_key = "***************************"
        bucket = "ravind"
        key = "ravind/dev-test-state/state.tf"
    }
}


# for main branch and gitlab pipelines, access and secret keys lines can be removed as they are found in default profile in the vm,
# In the variables section in the Gitlab CICD settings